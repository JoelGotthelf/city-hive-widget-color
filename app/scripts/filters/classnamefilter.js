'use strict';

/**
 * @ngdoc filter
 * @name cityHiveWidgetColorApp.filter:classNameFilter
 * @function
 * @description
 * # classNameFilter
 * Filter in the cityHiveWidgetColorApp.
 */
angular.module('cityHiveWidgetColorApp')
  .filter('classNameFilter', function ($filter) {
    return function (input) {
      return '$CH-' + input.replace( /^ch-(.*)/, '$1' );
    };
  });
