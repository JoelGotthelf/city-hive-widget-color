'use strict';

/**
 * @ngdoc overview
 * @name cityHiveWidgetColorApp
 * @description
 * # cityHiveWidgetColorApp
 *
 * Main module of the application.
 */
angular
  .module('cityHiveWidgetColorApp', [
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'color.picker'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
