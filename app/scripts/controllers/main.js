'use strict';

/**
 * @ngdoc function
 * @name cityHiveWidgetColorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cityHiveWidgetColorApp
 */
angular.module('cityHiveWidgetColorApp')
  .controller('MainCtrl', function ($scope) {
    $scope.classList = {
      'ch-black': null,
      'ch-yellow': null,
      'ch-header': null,
      'ch-background-alt': null,
      'ch-main-alt': null,
      'ch-white': null,
      'ch-merchant-name-color': null,
      'ch-discount-banner-alt': null,
      'ch-gray': null,
      'ch-darkgray': null,
      'ch-backgroud-light': null
    }
    $scope.applyAll = function(){
      $.each($scope.classList, function(k, v) {
        $scope.onColorChange(v, k);
      });
    }
    $scope.onColorChange = function(newColor, changeClass){
      if(!newColor){
        return;
      }
      cityHiveWidget.utils.remote_function.call('changeClassData',
        {name: changeClass, color: newColor});
      console.log(changeClass + ': ' + newColor);
    }
    var createFrame = function(){
      var widget_iframe = new cityHiveWidget.Iframe('city-hive-widget-app-iframe');
      widget_iframe.inject(
        document.getElementById('iframeContainer'),
        'http://localhost:8100/store_mobile.html#/',
        true);
      /*widget_iframe.inject(
        document.getElementById('iframeContainer'),
        'http://app.cityhive.net/winestore/store_mobile.html#/',
        true);*/
    }
    createFrame()
  });