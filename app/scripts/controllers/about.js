'use strict';

/**
 * @ngdoc function
 * @name cityHiveWidgetColorApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cityHiveWidgetColorApp
 */
angular.module('cityHiveWidgetColorApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
