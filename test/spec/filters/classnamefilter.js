'use strict';

describe('Filter: classNameFilter', function () {

  // load the filter's module
  beforeEach(module('cityHiveWidgetColorApp'));

  // initialize a new instance of the filter before each test
  var classNameFilter;
  beforeEach(inject(function ($filter) {
    classNameFilter = $filter('classNameFilter');
  }));

  it('should return the input prefixed with "classNameFilter filter:"', function () {
    var text = 'angularjs';
    expect(classNameFilter(text)).toBe('classNameFilter filter: ' + text);
  });

});
